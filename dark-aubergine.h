/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"black",     /* color 00: "black" */
	"red",       /* color 01: "red" */
	"green",     /* color 02: "green" */
	"yellow",    /* color 03: "yellow" */
	"#2E6EFA",   /* color 04: "blue" */
	"purple",    /* color 05: "purple" */
	"cyan",      /* color 06: "cyan" */
	"white",     /* color 07: "white" */

	/* 8 bright colors */
	"#4A4A4A",   /* color 08: "black" */
	"red",       /* color 09: "red" */
	"green",     /* color 10: "green" */
	"yellow",    /* color 11: "yellow" */
	"#5d8ffb",   /* color 12: "blue" */
	"purple",    /* color 13: "purple" */
	"cyan",      /* color 14: "cyan" */
	"white",     /* color 15: "white" */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#add8e6", /* cursor */
	"#555555", /* reverse cursor */
	"#ebdbb2", /* default foreground colour */
	"#240018", /* default background colour */
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
